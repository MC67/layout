;; Layout Bepo mod emacs
;; Copyright (C) 2023 Matthieu Cappe
;; Author: Matthieu Cappe
;; Version: 0.1
;; 
;; Utilisation dans layout de base bépo modifié afin d'avoir      
;; les 5 touches modifiantes sous les pouces. 
;; Voir description du layout pour plus d'information
;; Scan code source https://kbdlayout.info/KBDFR/scancodes+names

;; Touches modifiantes
;;
;; Notes
;; Les acces facile : 3489
;; Les touche à remap sont celle de la dernière colonne
;; c   echape      x
;; v   alt         :
;; b   shift       k
;; n   controle    ?
;; ,   super       q

v::LAlt  
n::Ctrl
c::escape
b::Shift
capslock::RAlt
SC032::AppsKey 

; Dans emacs, la touche , est mapper sur super


;; Les caractères
a::b
z::SC003
e::p
r::o
t::SC008
y::!
u::v
i::d
o::l
p::j
^::z
$::w
q::a
s::u
d::i
f::e
g::SC033
h::c
j::t
k::s
l::r
m::n
*::SC00A
SC028::m
;<::ê
w::SC00B
x::y
SC034::h
!::f
SC033::g
"::SC034
+"::?
'::x
SC00A::k
SC009::q
;<::^

;; Modification pour le confort
;; On garde la position de C-m pour agir comme la touche entrée.
^m::^m
 
;; On tire partie de la nouvelle position de AltGr
;; On continue d'envoyer espace
<^>!Space::Send {space}

;; Chiffres
<^>!SC003::Send {1}
<^>!"::Send {2}
<^>!'::Send {3}
<^>!(::Send {4}
<^>!z::Send {5}
<^>!e::Send {6}
<^>!r::Send {7}
<^>!t::Send {8}
<^>!d::Send {9} 
<^>!f::Send {0}

;; Ponctuation
<^>!g::Send {:}
<^>!l::Send {'}
<^>!h::Send {"}
<^>!j::Send {.}
<^>!k::Send {,}

;; Parentheses
<^>!v::Send {(}
<^>!n::Send {)}
<^>!c::Send {{}
<^>!SC032::Send {}}
<^>!x::Send {[}
<^>!SC033::Send {]}
<^>!p::Send {>}
<^>!m::Send {<}

;; Math
<^>!u::Send {+}
<^>!i::Send {=}
<^>!o::Send {-}

;; Other
<^>!y::Send {/}
<^>!SC028::Send {SC028}
